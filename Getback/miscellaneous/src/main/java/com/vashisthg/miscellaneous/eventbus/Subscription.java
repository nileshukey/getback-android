package com.vashisthg.miscellaneous.eventbus;



public class Subscription {
    public static final String SAVE_ITEM = "SAVE_ITEM";
    public static final String ITEM_SAVED = "ITEM_SAVED";

    public static final String FETCH_ITEM = "FETCH_ITEM";
    public static final String ITEM_FETCHED = "ITEM_FETCHED";

}
