package com.vashisthg.miscellaneous.eventbus;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import android.app.Application;
import android.content.Context;
import android.os.Handler;
import android.util.Log;


public class EventBus
{

	private static final String LOGTAG = EventBus.class.getName();
	private static final int MAX_THREADS = 5;

	private static ExecutorService executorService = Executors.newFixedThreadPool(MAX_THREADS);

	public enum RunInThread
	{
		AUTO, PRIVATE, UI
	};

	private static HashMap<String, HashMap<String, Listener>> hmListeners = new HashMap<String, HashMap<String, Listener>>();

	private static class Listener
	{

		public Object instance;
		public Class<?> classToInstantiate;
		public Method method;
		public int numOfParameters;
		public Context context;
		public RunInThread runInThread;

		public String uniqueKey;

		public Listener(Method method, Class classToInstantiate, int numOfParameters)
		{
			this.method = method;
			this.classToInstantiate = classToInstantiate;
			this.numOfParameters = numOfParameters;
			this.runInThread = RunInThread.PRIVATE;
			this.uniqueKey = System.identityHashCode(classToInstantiate.getName()) + "_" + method.getName() + "_" + numOfParameters;
		}

		public Listener(Method method, Object instance, int numOfParameters, RunInThread runInThread)
		{
			this.method = method;
			this.instance = instance;
			this.numOfParameters = numOfParameters;
			if (runInThread == RunInThread.AUTO || runInThread == RunInThread.UI)
				this.context = instance instanceof Context ? (Context) instance : null;
			this.runInThread = runInThread;
			this.uniqueKey = System.identityHashCode(instance) + "_" + method.getName() + "_" + numOfParameters;
		}

		public void callListener() throws IllegalArgumentException, IllegalAccessException, InvocationTargetException, InstantiationException
		{
			if (instance == null && classToInstantiate != null)
				instance = classToInstantiate.newInstance();
			method.invoke(instance);
		}

		public void callListener(Object[] objects) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException, InstantiationException
		{
			if (instance == null && classToInstantiate != null)
				instance = classToInstantiate.newInstance();
			method.invoke(instance, objects);
		}

	}

	private static void addToListeners(String subscribeId, Method method, Class classToInstantiate, int numOfParameters)
	{
		Listener listener = new Listener(method, classToInstantiate, numOfParameters);
		if (hmListeners.containsKey(subscribeId))
		{
			if (!hmListeners.get(subscribeId).containsKey(listener.uniqueKey))
			{
				hmListeners.get(subscribeId).put(listener.uniqueKey, listener);
			}
		} else
		{
			HashMap<String, Listener> hmListener = new HashMap<String, EventBus.Listener>();
			hmListener.put(listener.uniqueKey, listener);
			hmListeners.put(subscribeId, hmListener);
		}
	}

	private static void addToListeners(String subscribeId, Method method, Object instance, int numOfParameters, RunInThread runInThread)
	{
		Listener listener = new Listener(method, instance, numOfParameters, runInThread);
		if (hmListeners.containsKey(subscribeId))
		{
			if (!hmListeners.get(subscribeId).containsKey(listener.uniqueKey))
			{
				hmListeners.get(subscribeId).put(listener.uniqueKey, listener);
			}
		} else
		{
			HashMap<String, Listener> hmListener = new HashMap<String, EventBus.Listener>();
			hmListener.put(listener.uniqueKey, listener);
			hmListeners.put(subscribeId, hmListener);
		}
	}

	private static class AsyncListenerCall implements Runnable
	{

		private Listener listener = null;
		private Object[] objects = null;

		public AsyncListenerCall(Listener listener, Object[] objects)
		{
			this.listener = listener;
			this.objects = objects;
		}

		@Override
		public void run()
		{
			try
			{
				if (objects == null)
					listener.callListener();
				else
					listener.callListener(objects);
			} catch (InvocationTargetException ite)
			{
				Log.e(LOGTAG, "Error publishing event to method " + listener.method.getName() + "\nin class " + listener.instance.getClass().getName() + "\n" + ite.getCause().toString());
			} catch (Exception ex)
			{
				Log.e(LOGTAG, "Error publishing event to method " + listener.method.getName() + "\nin class " + listener.instance.getClass().getName() + "\n" + ex.getMessage() + "\n");
			}

		}

	}

	public static void dumpSubscribers()
	{
		Log.d(LOGTAG, "Dumping Subscribers");
		Set<Entry<String, HashMap<String, Listener>>> set = hmListeners.entrySet();
		Iterator<Entry<String, HashMap<String, Listener>>> i = set.iterator();
		while (i.hasNext())
		{
			Map.Entry subscriber = (Map.Entry) i.next();
			Log.d(LOGTAG,"Subscribers for "+subscriber.getKey());
			{
				ArrayList<Listener> listeners = new ArrayList<EventBus.Listener>();
				for (Listener listener : hmListeners.get(subscriber.getKey()).values())
				{
					Log.d(LOGTAG,"Instance "+listener.instance+" ("+listener.uniqueKey+")");
				}
			}
		}

	}

	public static void publishEvent(String subscribeId, Object... parameters)
	{
		Log.d(LOGTAG, "publishEvent - " + subscribeId);

		List<Listener> listeners = getListenersById(subscribeId, parameters);
		boolean foundReceiver = false;
		if (listeners != null)
		{
			for (Listener listener : listeners)
			{
				AsyncListenerCall aSyncListenerCall = new AsyncListenerCall(listener, parameters);
				if (listener.context == null || listener.runInThread == RunInThread.PRIVATE)
				{
					executorService.execute(aSyncListenerCall);
					foundReceiver = true;
				} else
				{
					new Handler(listener.context.getMainLooper()).post(aSyncListenerCall);
					foundReceiver = true;
				}
			}
		}
		if (!foundReceiver)
        {
			Log.w(LOGTAG, "No receiver for Event " + subscribeId + " found. Please verify receiver parameters.");
        }
	}

	private static List<Listener> getListenersById(String subscribeId, Object... parameters)
	{
		if (hmListeners.containsKey(subscribeId))
		{
			ArrayList<Listener> listeners = new ArrayList<EventBus.Listener>();
			for (Listener listener : hmListeners.get(subscribeId).values())
			{
				if (listener.numOfParameters == parameters.length)
					listeners.add(listener);
			}
			return listeners.size() == 0 ? null : listeners;
		} else
			return null;
	}

	public static void addClass(Class<?> classToInstantiate)
	{
		Method[] methods = classToInstantiate.getDeclaredMethods();
		for (Method method : methods)
		{
			Annotation[] annotations = method.getDeclaredAnnotations();
			for (Annotation annotation : annotations)
			{
				if (annotation.annotationType().equals(Event.class))
				{
					Class<?>[] parameterTypes = method.getParameterTypes();
					addToListeners(((Event) method.getAnnotation(annotation.annotationType())).subscribeId(), method, classToInstantiate, parameterTypes.length);
				}
			}
		}
	}

	/**
	 * This will register the object instance to the EventBus.
	 * It will handle events that are put in the abstract layer.
	 * For that you need to call addInstance with the includeAbstract
	 * parameter
	 * @param instance
	 */
	public static void addInstance(Object instance)
	{
		addInstance(instance, false);
	}
	
	/**
	 * This will register the object instance to the EventBus.
	 * @param instance
	 * @param includeAbstract Set to true to allow the abstract layer to receive Events.
	 */
	public static void addInstance(Object instance, boolean includeAbstract)
	{
		synchronized (hmListeners)
		{
			Method[] methods;
			
			Class<? extends Object> objectClass = instance.getClass();
			if(!includeAbstract)
				methods = objectClass.getDeclaredMethods();
			else
				methods = objectClass.getMethods();
			
			for (Method method : methods)
			{
				Annotation[] annotations = method.getDeclaredAnnotations();
				for (Annotation annotation : annotations)
				{
					if (annotation.annotationType().equals(Event.class))
					{
						Class<?>[] parameterTypes = method.getParameterTypes();
						addToListeners(((Event) method.getAnnotation(annotation.annotationType())).subscribeId(), method, instance, parameterTypes.length, ((Event) method.getAnnotation(annotation.annotationType())).runInThread());
					}
				}
			}
		}
	}
	


	public static void removeInstance(Object instance)
	{
		synchronized (hmListeners)
		{
			for (HashMap<String, Listener> hmListener : hmListeners.values())
			{
				for (String key : hmListener.keySet())
				{
					Listener listener = hmListener.get(key);
					if (listener.instance.equals(instance))
					{
						hmListener.remove(key);
						removeInstance(instance);
						return;
					}
				}
			}
			return;
		}
	}

}
