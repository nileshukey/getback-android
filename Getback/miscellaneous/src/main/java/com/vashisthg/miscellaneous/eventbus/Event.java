package com.vashisthg.miscellaneous.eventbus;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;



@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Event
{
	String subscribeId() default "";

	EventBus.RunInThread runInThread() default EventBus.RunInThread.AUTO;
}