package com.vashisthg.getback.model;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by vashisthg on 14/05/14.
 */

@DatabaseTable(tableName = "person")
public class Person {

    @DatabaseField
    private String firstName;

    @DatabaseField
    private String lastName;

    @DatabaseField(id = true)
    private String personRecordId;

    @DatabaseField
    private String savedName;

    @ForeignCollectionField
    private ForeignCollection<Email> emails;

    @DatabaseField(foreign = true)
    private Item item;

    @ForeignCollectionField
    private ForeignCollection<Phone> phones;


    public Person() {

    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPersonRecordId() {
        return personRecordId;
    }

    public void setPersonRecordId(String personRecordId) {
        this.personRecordId = personRecordId;
    }

    public String getSavedName() {
        return savedName;
    }

    public void setSavedName(String savedName) {
        this.savedName = savedName;
    }

    public ForeignCollection<Email> getEmails() {
        return emails;
    }

    public void setEmails(ForeignCollection<Email> emails) {
        this.emails = emails;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public ForeignCollection<Phone> getPhones() {
        return phones;
    }

    public void setPhones(ForeignCollection<Phone> phones) {
        this.phones = phones;
    }
}
