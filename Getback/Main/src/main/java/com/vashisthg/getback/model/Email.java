package com.vashisthg.getback.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by vashisthg on 14/05/14.
 */

@DatabaseTable(tableName = "email")
public class Email {

    @DatabaseField(id = true)
    private String email;

    @DatabaseField
    private String emailLabel;

    @DatabaseField(foreign = true)
    private Person person;

    public Email() {

    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmailLabel() {
        return emailLabel;
    }

    public void setEmailLabel(String emailLabel) {
        this.emailLabel = emailLabel;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }
}
