package com.vashisthg.getback.application;

import android.app.Application;

/**
 * Created by vashisthg on 15/05/14.
 */
public class GetBackApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        DataManager.initInstance(this, "get_back" );

    }
}
