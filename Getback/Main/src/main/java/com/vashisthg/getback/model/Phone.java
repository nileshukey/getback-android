package com.vashisthg.getback.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by vashisthg on 14/05/14.
 */
@DatabaseTable(tableName = "phone")
public class Phone {

    @DatabaseField
    private String phoneLabel;

    @DatabaseField
    private String phoneNumber;

    @DatabaseField(foreign = true)
    private Person person;

    public String getPhoneLabel() {
        return phoneLabel;
    }

    public void setPhoneLabel(String phoneLabel) {
        this.phoneLabel = phoneLabel;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }
}
