package com.vashisthg.getback.model;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by vashisthg on 13/05/14.
 */

@DatabaseTable(tableName = "user")
public class User {

    @DatabaseField
    private String email;

    @DatabaseField(id = true)
    private String userName;

    @DatabaseField(foreign = true, canBeNull = true)
    private Item archivedItems;

    @ForeignCollectionField
    private ForeignCollection<Item> items;

    public User() {

    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Item getArchivedItems() {
        return archivedItems;
    }

    public void setArchivedItems(Item archivedItems) {
        this.archivedItems = archivedItems;
    }

    public ForeignCollection<Item> getItems() {
        return items;
    }

    public void setItems(ForeignCollection<Item> items) {
        this.items = items;
    }
}
