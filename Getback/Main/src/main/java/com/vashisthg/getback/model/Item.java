package com.vashisthg.getback.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.vashisthg.miscellaneous.dbcache.AbstractOrmLiteClass;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;


/**
 * Created by vashisthg on 14/05/14.
 */
@DatabaseTable(tableName = "item")
public class Item extends AbstractOrmLiteClass<Item> {

    private static final String COLUMN_ID = "ITEM_ID";

    @DatabaseField
    private int colorCode;

    @DatabaseField
    private Date createdAt;

    @DatabaseField
    private String givenToContactName;

    @DatabaseField
    private boolean gotBack;

    @DatabaseField
    private String iconName;

    @DatabaseField
    private String imageUrl;

    @DatabaseField(generatedId = true, columnName = COLUMN_ID)
    private long itemId;

    @DatabaseField
    private String itemTitle;

    @DatabaseField
    private String itemType;

    @DatabaseField
    private String linkedABRecordId;

    public Item(int colorCode, String givenToContactName, boolean gotBack, String iconName, String imageUrl, String itemTitle, String itemType, String linkedABRecordId, String localNotificationId, String notes, Date returnDate, String syncStatus, User user, Person person) {
        this.colorCode = colorCode;
        this.givenToContactName = givenToContactName;
        this.gotBack = gotBack;
        this.iconName = iconName;
        this.imageUrl = imageUrl;
        this.itemTitle = itemTitle;
        this.itemType = itemType;
        this.linkedABRecordId = linkedABRecordId;
        this.localNotificationId = localNotificationId;
        this.notes = notes;
        this.returnDate = returnDate;
        this.syncStatus = syncStatus;
        this.user = user;
        this.person = person;
    }

    @DatabaseField
    private String localNotificationId;

    @DatabaseField
    private Date modifiedAt;

    @DatabaseField
    private String notes;

    @DatabaseField
    private Date returnDate;

    @DatabaseField
    private String syncStatus;

    @DatabaseField(foreign = true)
    private User user;

    @DatabaseField(foreign = true)
    private Person person;

    public Item() {

    }

    public int getColorCode() {
        return colorCode;
    }

    public void setColorCode(int colorCode) {
        this.colorCode = colorCode;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getGivenToContactName() {
        return givenToContactName;
    }

    public void setGivenToContactName(String givenToContactName) {
        this.givenToContactName = givenToContactName;
    }

    public boolean getGotBack() {
        return gotBack;
    }

    public void setGotBack(boolean gotBack) {
        this.gotBack = gotBack;
    }

    public String getIconName() {
        return iconName;
    }

    public void setIconName(String iconName) {
        this.iconName = iconName;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public long getItemId() {
        return itemId;
    }

    public void setItemId(long itemId) {
        this.itemId = itemId;
    }

    public String getItemTitle() {
        return itemTitle;
    }

    public void setItemTitle(String itemTitle) {
        this.itemTitle = itemTitle;
    }

    public String getItemType() {
        return itemType;
    }

    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    public String getLinkedABRecordId() {
        return linkedABRecordId;
    }

    public void setLinkedABRecordId(String linkedABRecordId) {
        this.linkedABRecordId = linkedABRecordId;
    }

    public String getLocalNotificationId() {
        return localNotificationId;
    }

    public void setLocalNotificationId(String localNotificationId) {
        this.localNotificationId = localNotificationId;
    }

    public Date getModifiedAt() {
        return modifiedAt;
    }

    public void setModifiedAt(Date modifiedAt) {
        this.modifiedAt = modifiedAt;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Date getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(Date returnDate) {
        this.returnDate = returnDate;
    }

    public String getSyncStatus() {
        return syncStatus;
    }

    public void setSyncStatus(String syncStatus) {
        this.syncStatus = syncStatus;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    @Override
    protected void onBeforeSave() {
        if (null == createdAt) {
            createdAt = new Date();
        }
        modifiedAt = new Date();
    }

    @Override
    protected void onAfterSave() {

    }

    public static List<Item> getItemById(String gameId) {
        try {
            return new Item().queryManyByEqual(COLUMN_ID, gameId, COLUMN_ID, true);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }


    @Override
    public String toString() {
        return "id: " + itemId + "itemTitle: " + itemTitle;
    }
}
