package com.vashisthg.getback.application;

import android.app.Application;
import android.util.Log;

import com.vashisthg.getback.R;
import com.vashisthg.getback.model.Item;
import com.vashisthg.miscellaneous.dbcache.AbstractOrmLiteClass;
import com.vashisthg.miscellaneous.dbcache.OrmLiteDatabaseHelper;
import com.vashisthg.miscellaneous.eventbus.Event;
import com.vashisthg.miscellaneous.eventbus.EventBus;
import com.vashisthg.miscellaneous.eventbus.Subscription;

import java.io.InputStream;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by vashisthg on 15/05/14.
 */
public class DataManager implements OrmLiteDatabaseHelper.Upgrader {



    private static final String LOGTAG = DataManager.class.getName();

    private static final int DB_VERSION = 2;

    private static DataManager instance;
    private Application appContext;

    public static void initInstance(Application appContext, String databaseName)
    {
        instance = new DataManager();
        instance.appContext = appContext;
        AbstractOrmLiteClass.initDB(appContext, databaseName, DB_VERSION, instance);
        EventBus.addInstance(instance);
    }

    public static DataManager getInstance() {
        return instance;
    }

    @Override
    public InputStream getUpgradeStream(int oldVersion, int newVersion)
    {
        try
        {
            InputStream inputStream = null;
            if (oldVersion == 1 && newVersion == 2)
                inputStream = appContext.getResources().openRawResource(R.raw.dbupgrade_1_to_2);

            if (inputStream == null)
            {
                Log.e(LOGTAG, "Can't upgrade the database. Please update the getUpgradeStream method in DBCacheManager class. Will now grab the script found in R.raw.dbupgrade_fallback resource");
                inputStream = appContext.getResources().openRawResource(R.raw.dbupgrade_fallback);
            }

            return inputStream;
        } catch (Exception ex)
        {
            ex.printStackTrace();
            return null;
        }
    }


    @Event(subscribeId = Subscription.SAVE_ITEM)
    public void addItem(Item item) {
        try {
            item.save();
            EventBus.publishEvent(Subscription.ITEM_SAVED, item);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Event(subscribeId = Subscription.FETCH_ITEM)
    public void fetchAllItems() {
        try {
            List<Item> items = new Item().getAll();
            EventBus.publishEvent(Subscription.ITEM_FETCHED, items);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }






}
